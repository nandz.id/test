<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table ='karyawan';
    protected $fillable = [
        'nama_karyawan',
        'jk',
        'jabatan_id',
        'alamat'
    ];

    // public function jabatan()
    // {
    // 	return $this->hasOne('App\jabatan');
    // }
}
