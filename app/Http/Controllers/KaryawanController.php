<?php

namespace App\Http\Controllers;
// namespace App\Models;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Jabatan;
use DB;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $karyawan = DB::table('karyawan as k')
                    ->leftJoin('jabatan as j', 'k.jabatan_id', '=', 'j.id')
                    ->select('k.id','k.nama_karyawan','k.jk','j.nama_jabatan','k.alamat')
                    ->orderBy('k.id', 'asc')
                    ->paginate(2);
        // $karyawan = Karyawan::all();
        // print_r($karyawan);
        // die();
        return view ('karyawan.index',compact('karyawan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jabatan = Jabatan::all();
        return view('karyawan.create',compact('jabatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Karyawan;
        $book->nama_karyawan = $request->get('nama_karyawan');
        $book->jk            = $request->get('jk');
        $book->jabatan_id    = $request->get('jabatan');
        $book->alamat        = $request->get('alamat');
        $book->save();
        
        return redirect('karyawans')->with('success', 'Data karyawan telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $karyawan = Karyawan::find($id);
        $jabatan  = Jabatan::all();
        return view('karyawan.edit',compact('karyawan','jabatan', 'id'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $karyawan = Karyawan::find($id);
        $karyawan->nama_karyawan = $request->get('nama_karyawan');
        $karyawan->jabatan_id    = $request->get('jabatan');
        $karyawan->jk            = $request->get('jk');
        $karyawan->alamat        = $request->get('alamat');
        $karyawan->save();
        return redirect('karyawans')->with('success', 'Data Karyawans sukses di update');      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $karyawan = Karyawan::find($id);
        $karyawan->delete();
        return redirect('karyawans')->with('success','Data Karyawan telah di hapus');
    }
}
