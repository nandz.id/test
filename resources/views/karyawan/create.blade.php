<!-- create.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Laravel 5.7 CRUD Tutorial With Example  </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Tambah Karyawan</h2><br/>
      <form method="post" action="{{url('karyawans')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="nama_karyawan">Nama Karyawan:</label>
            <input type="text" class="form-control" name="nama_karyawan">
          </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="jk">Jenis Kelamin:</label>
                 <select name="jk">
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option> 
                 </select>
            </div>
        </div>

        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label>Jabatan:</label>
                <select name="jabatan">
                    @foreach ($jabatan as $val)
                        <option value="{{ $val['id'] }}">{{ $val['nama_jabatan'] }}</option>  
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Qty">Alamat:</label>
              <textarea name="alamat"></textarea>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>