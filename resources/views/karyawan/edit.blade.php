<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Laravel 5.7 CRUD</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Edit Data Karyawan</h2><br/>
      <form method="post" action="{{action('KaryawanController@update', $id)}}">
      @csrf
      <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="nama_karyawan">Nama Karyawan:</label>
            <input type="text" class="form-control" name="nama_karyawan" value="{{$karyawan['nama_karyawan']}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label>Jenis Kelamin:</label>
                    <select name="jk">
                        <option value="L" @if($karyawan['jk']=="L") selected @endif>Laki-laki</option>
                        <option value="P" @if($karyawan['jk']=="P") selected @endif>Perempuan</option>
                    </select>
                </div>
            </div>
        </div>
         <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label>Jabatan:</label>
                <select name="jabatan">
                    @foreach ($jabatan as $val)
                        <option value="{{ $val['id'] }}" @if($karyawan['jabatan_id']==$val['id']) selected @endif>{{$val['nama_jabatan']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="alamat">Alamat</label>
                <textarea name="alamat">{{$karyawan['alamat']}}</textarea>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>